const render = (txt: string[]) => {
  console.clear()
  txt.forEach((line) => console.log(line))
}

export { render }
