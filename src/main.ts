import { render } from './render.ts'
import { getEyeMatrix } from './get-eye-matrix.ts'
import { takeStepInMatrixOneDirectionAtATime } from './matrix-stepper/index.ts'
import { wait } from './common/index.ts'

const eyeMatrix = await getEyeMatrix()
const getIndexesForNextFrame = takeStepInMatrixOneDirectionAtATime(
  50,
  70
)(eyeMatrix)

let lastFrame: [number, number] = [0, 0]
let currentFrame: [number, number] = [0, 0]

while (true) {
  const [y, x] = getIndexesForNextFrame(lastFrame, currentFrame)

  lastFrame = currentFrame
  currentFrame = [y, x]
  render(eyeMatrix[y][x])
  await wait(250)
}
