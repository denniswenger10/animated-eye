const getViaNetwork = async (url: URL) => {
  const resp = await fetch(url)
  const data = await resp.json()
  return data
}

const getLocally = async (url: URL) => {
  const decoder = new TextDecoder('utf-8')
  const file = await Deno.readFile(url)

  return JSON.parse(decoder.decode(file))
}

const readLocalJson = async (path: string) => {
  const url = new URL(path, import.meta.url)

  return url.protocol === 'file'
    ? getLocally(url)
    : getViaNetwork(url)
}

export { readLocalJson }
