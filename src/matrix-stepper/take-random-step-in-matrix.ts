import { clampNumber, randomNumberBetween } from '../common/index.ts'

/**
 * Basically completely random
 */
const takeRandomStepsInMatrix = (stepLength: number = 1) => (
  matrix: any[][]
) => (currentPosition: [number, number]): [number, number] => {
  const [y, x] = currentPosition.map((direction) =>
    randomNumberBetween(
      direction - stepLength,
      direction + stepLength
    )
  )

  return [
    clampNumber(0, matrix.length - 1, y),
    clampNumber(0, matrix[0].length - 1, x),
  ]
}

export { takeRandomStepsInMatrix }
