import { randomNumberBetween } from '../common/index.ts'

/**
 * Maybe a nicer function than complete randomness
 */
const takeStepInMatrixOneDirectionAtATime = (
  probabilityToChange: number = 50,
  probabilityToGoHorizontal: number = 70
) => (matrix: any[][]) => (
  [lastY]: [number, number],
  [currentY, currentX]: [number, number]
): [number, number] => {
  if (currentY > 0) {
    return [
      currentY + 1 >= matrix.length || lastY > currentY
        ? currentY - 1
        : currentY + 1,
      currentX,
    ]
  }

  const shouldChange =
    randomNumberBetween(0, 100) > probabilityToChange

  if (!shouldChange) return [currentY, currentX]

  const shouldGoHorizontal =
    randomNumberBetween(0, 100) > probabilityToGoHorizontal

  return shouldGoHorizontal
    ? [currentY + 1, currentX]
    : [
        currentY,
        randomNumberBetween(0, 1) < 1
          ? currentX + 1 >= matrix[0].length
            ? currentX - 1
            : currentX + 1
          : currentX - 1 <= 0
          ? currentX + 1
          : currentX - 1,
      ]
}

export { takeStepInMatrixOneDirectionAtATime }
