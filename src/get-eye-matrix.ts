import { readLocalJson } from './read-local-json.ts'

const getEyeMatrix = async (): Promise<string[][][]> => {
  const eyeData = await readLocalJson('data/milton.json')
  const halfStates = Object.values(eyeData.half)
  const openStates = Object.values(eyeData.open)

  return [
    openStates,
    halfStates,
    halfStates.map(() => eyeData.closed),
  ]
}

export { getEyeMatrix }
