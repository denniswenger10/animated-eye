const clampNumber = (min: number, max: number, num: number) =>
  Math.max(Math.min(num, Math.max(min, max)), Math.min(min, max))

export { clampNumber }
