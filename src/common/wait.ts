const wait = (timeInMs: number) =>
  new Promise((resolve) => setTimeout(resolve, timeInMs))

export { wait }
